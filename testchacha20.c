#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <openssl/evp.h>

/**
 * Interdasting:
 * - https://stackoverflow.com/a/13045182
 * - https://www.openssl.org/docs/man1.1.1/man3/EVP_EncryptInit.html,
 *   section ChaCha20-Poly1305
 *
 * Tips:
 * - key_length=32
 * - iv_length=12
 * - tag length=16
 *
 * For ChaCha20-Poly1305: iv == nonce
 */

static const uint8_t key[] = {0xAD, 0xCC, 0xB6, 0xB6, 0x5A, 0xDE, 0x2D, 0x39, 0xEE, 0xCB, 0x1D, 0x2C, 0x17, 0x4A, 0x77, 0x78, 0xD5, 0x93, 0xCE, 0xEF, 0xBC, 0x9E, 0x8F, 0x4B, 0x51, 0x7A, 0x56, 0xE1, 0x3C, 0x6B, 0xE1, 0x12};
static const uint8_t iv[] = {0xDA, 0x7B, 0xD6, 0xAE, 0x5D, 0x67, 0xFC, 0x95, 0x60, 0xDA, 0x9D, 0xCA};

static void print_array(const char *name, const uint8_t *buf, size_t len)
{
	size_t i;

	printf("%s: ", name);
	for (i = 0; i < len; i++) {
		printf("0x%02X", buf[i]);
		if (i < len - 1)
			printf(", ");
	}

	printf("\n");
}

#if 0
static void build_key(const char *name, int len)
{
	uint8_t buf[64];
	int fd;
	int i;
	ssize_t read_ret;

	assert(len <= sizeof(buf));

	fd = open("/dev/urandom", O_RDONLY);
	assert(fd != -1);

	read_ret = read(fd, buf, len);
	assert(read_ret == len);

	printf("static const uint8_t %s[] = {", name);
	for (i = 0; i < len; i++) {
		printf("0x%02X", buf[i]);
		if (i < len -1)
			printf(", ");
	}

	printf("};\n");
}
#endif

#if 0
int main(int argc, char *argv[])
{
	build_key("key", 32);
	build_key("iv", 12);

	return 0;
}
#endif

#if 0
int main(int argc, char *argv[])
{
	const unsigned char text[] = "Hello world!";

	const EVP_CIPHER *cipher;
	EVP_CIPHER_CTX *ctx;
	uint8_t tag[16]; // 128 bits
	uint8_t *out;
	int out_size;
	int out_len;
	int wrote;
	int ret;

	const uint8_t *in = text;
	const size_t in_len = sizeof(text);

	out_len = sizeof(text);
	out = malloc(out_len);
	assert(out);

	cipher = EVP_chacha20_poly1305();
	ctx = EVP_CIPHER_CTX_new();

	assert(EVP_CIPHER_key_length(cipher) == sizeof(key));
	assert(EVP_CIPHER_iv_length(cipher) == sizeof(iv));

	ret = EVP_EncryptInit(ctx, cipher, key, iv);
	assert(ret);

	ret = EVP_EncryptUpdate(ctx, out, &out_len, in, in_len);
	assert(ret);

	// Should produce nothing in chacha20. Dirty hack
	int fake = 100;
	ret = EVP_EncryptFinal(ctx, out, &fake);
	assert(ret);
	assert(fake == 0);

	ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, sizeof(tag), tag);
	assert(ret);

	printf("In len=%zu, out len=%d\n", in_len, out_len);
	print_array("In", in, in_len);
	print_array("Out", out, out_len);
	print_array("Tag", tag, sizeof(tag));

	EVP_CIPHER_CTX_free(ctx);

	free(out);

	return 0;
}
#endif

#if 1
int main(int argc, char *argv[])
{
	const uint8_t in[] = { 0x4E, 0x52, 0x75, 0xC0, 0xB9, 0x53, 0x15, 0xA9, 0xD2, 0x38, 0x26, 0x17, 0xF5 };
	const uint8_t tag[] = { 0xCA, 0x85, 0xC6, 0x2A, 0x14, 0x04, 0x90, 0x1E, 0xBE, 0x6A, 0x45, 0x1B, 0xBB, 0x52, 0x1B, 0x2F };

	uint8_t *out;
	int out_size;
	int out_len;

	const size_t in_len = sizeof(in);

	out_len = sizeof(in_len);
	out = malloc(out_len);
	assert(out);

	const EVP_CIPHER *cipher;
	EVP_CIPHER_CTX *ctx;
	int ret;

	cipher = EVP_chacha20_poly1305();
	ctx = EVP_CIPHER_CTX_new();

	ret = EVP_DecryptInit(ctx, cipher, key, iv);
	assert(ret);

	ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, sizeof(tag), tag);
	assert(ret);

	ret = EVP_DecryptUpdate(ctx, out, &out_len, in, in_len);
	assert(ret);

	// Should produce nothing in chacha20. Dirty hack
	int fake = 100;
	ret = EVP_DecryptFinal(ctx, out, &fake);
	assert(ret);
	assert(fake == 0);

	// Expected out: 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21, 0x00
	printf("In len=%zu, out len=%d\n", in_len, out_len);
	print_array("In", in, in_len);
	print_array("Out", out, out_len);

	EVP_CIPHER_CTX_free(ctx);

	free(out);

	return 0;
}
#endif
